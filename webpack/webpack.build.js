var loaders = require("./loaders");
var preloaders = require("./preloaders");
var HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
var webpack = require('webpack');

module.exports = {
  entry: ['./src/index.ts'],
  output: {
    filename: 'build.js',
    path: 'public',
  },
  devtool: 'source-map',
  resolve: {
    root: __dirname,
    extensions: ['', '.ts', '.js', '.json'],
  },
  resolveLoader: {
    modulesDirectories: ["node_modules"]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      warning: false,
      mangle: true,
      comments: false,
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      inject: 'body',
      hash: true,
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      'window.jquery': 'jquery',
    }),
   new CopyWebpackPlugin([{
      from: 'src/assets',
      to: '../public/assets',
    },
    {
     from: 'src/data',
     to: '../public'
        },]),

    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production'),
        'API_URL': JSON.stringify('https://intense-basin-83150.herokuapp.com/')
      },
    }),
  ],
  module: {
    preLoaders: preloaders,
    loaders: loaders,
  },
  tslint: {
    emitErrors: true,
    failOnHint: false,
  },
};
