import * as angular from 'angular';
import { viewport } from './viewport.component';

export const viewportModule = angular
  .module('app.main.containers.viewport', [])
  .component('viewport', viewport)
  .name;
