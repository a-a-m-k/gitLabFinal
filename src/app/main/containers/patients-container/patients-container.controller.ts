import * as _ from 'lodash';
import { PatientsContainerService } from './patients-container.service';
import { Patient } from '../../components/add-patient/add-patient.interface';

export class PatientsContainerController {
  static $inject = ['PatientsContainerService', '$timeout'];
  patients;
  selectedPatient;
  filteredPatients;
  newPatient: Patient;
  message;

  constructor(
    private patientsContainerService: PatientsContainerService,
    private timeout) {};

  $onInit() {
    this.newPatient = this.patientsContainerService.getDefaultPaient();
    this.message = '';
    this.getPatients();
  }

  private getPatients() {
    this.patientsContainerService.getPatients()
      .then(response => {
        this.patients = response.data;
        this.filteredPatients = response.data;
        this.selectedPatient = this.patients[0];
      });
  }

  private addPatient(patient) {
    const id = _.isEmpty(this.patients) ? 0 : _.last(this.patients)['id'] + 1;
    this.patients = this.patients.concat({ ...patient, id });
    this.filteredPatients = [...this.patients];
    this.newPatient = this.patientsContainerService.getDefaultPaient();
  }
  
  private isDuplicated(list, item) {
    const { firstName, lastName } = item;
    return _.some(list, { firstName, lastName });
  }

  selectPatient(index) {
    this.selectedPatient = this.patients.find(patient => patient.id === index);
  }

  changeFilter(res) { 
    this.timeout(() => {
      this.filteredPatients = res;
    });
  }

  createPatient({ patient }) {
    const isDuplicated = this.isDuplicated(this.patients, patient);
    this.message = isDuplicated ? 'This user is already exist' : 'Patient was successfully registered';
    !isDuplicated && this.addPatient(patient);
  }

  hideMessage() {
    this.message = '';
  }

}

