import * as angular from 'angular';
import { PatientsContainerComponent } from './patients-container.component';
import { PatientsContainerService } from './patients-container.service';

export const patientsContainerModule = angular
  .module('app.main.patients-container', [])
  .component('patientsContainerComponent', PatientsContainerComponent)
  .service('PatientsContainerService', ['$http', PatientsContainerService])
  .name;
