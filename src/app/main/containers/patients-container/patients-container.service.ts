export class PatientsContainerService {
  
  constructor(private $http) {};

  public getPatients() {
    return this.$http.get('patients.json');
  }

  public getDefaultPaient() {
    return {
      id: 0,
      email: '',
      firstName: '',
      lastName: '',
      sex: '',
      birthData: '',
      deathData: '',
      business: '',
      city: '',
      state: '',
      street: '',
      house: '',
      zip: '',
      phone: '',
      workPhone: '',
      submissionDate: '',
      comments: ''
    };
  }
}

