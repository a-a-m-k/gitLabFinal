import * as angular from 'angular';
import { Main } from './main.component';
import { mainRoutes } from './main.config';
import { componentsModule } from './components/components.module';
import { containersModule } from './containers/containers.module';
import { menuModule } from './common/menu/menu.module';

export const mainModule = angular
  .module('app.main', [
    componentsModule,
    containersModule,
    menuModule
  ])
  .component('main', Main)
  .config(mainRoutes)
  .name;
