
export class TabsList {

  static get Default(): any[] {
    return [
             {
               tabName: 'Info'
             },
             {
              tabName: 'Meds'
             },
             {
              tabName: 'Problems'
             }
            ];
  }
}
