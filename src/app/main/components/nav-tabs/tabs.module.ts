import * as angular from 'angular';
import { tabsRoutes } from './tabs.config';
import { TabsComponent } from './tabs.component';
import { TabsList } from './tabs.constant';
import { medsModule } from '../meds/meds.module';
import { problemsModule } from '../problems/problems.module';
import { patientInfoModule } from '../patient-info/patient-info.module';

export const tabsModule = angular
  .module('app.main.tabs', [
    patientInfoModule,
    medsModule,
    problemsModule
  ])
  .component('tabsComponent', TabsComponent)
  .constant('TabsProperties', TabsList.Default)
  .config(tabsRoutes)
  .name;
