tabsRoutes.$inject = ['$stateProvider'];

export function tabsRoutes($stateProvider: any) {
  $stateProvider.state('tabs', {
    url: '/tabs',
    component: 'tabs'
  });
}
