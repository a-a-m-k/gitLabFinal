import './tabs.component.scss';

import { TabsController } from './tabs.controller';

export const TabsComponent: angular.IComponentOptions = {
  bindings: { patient: '<' },
  template: require('./tabs.component.html'),
  controller: TabsController
};

