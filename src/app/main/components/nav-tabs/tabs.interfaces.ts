
export interface Tab {
  tabName: string
}

export interface Tabs {
  tabs: Tab[]
}
