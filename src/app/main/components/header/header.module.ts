import * as angular from 'angular';
import { headerComponent } from './header.component';


export const headerModule = angular
  .module('app.main.header', [])
  .component('headerComponent', headerComponent)  
  .name;

