
export class WidgetTableService {

  getGridOptions(widget, index, property, namebtn) {
    return { 
      enableCellEditOnFocus: true,
      enableHorizontalScrollbar: 0,
      enableVerticalScrollbar: 0,
      rowHeight: 110,
      enableSorting: false,
      enableColumnMenus: false,
      columnDefs: this.getColumnDefs(widget, index, property, namebtn)
    };
  }

  getColumnDefs(widget, index, property, namebtn) {
    return [
      {
        name: `${widget}[${index}].date`, 
        enableCellEdit: false, 
        displayName: 'date', 
        width: 85, 
        cellClass: 'blue', 
        enableHiding: false
      },
      {
        name: `${widget}[${index}].${property}`, 
        enableCellEditOnFocus: false, 
        displayName: property, 
        width: 150, 
        enableHiding: false
      }
    ];
  } 
}

