import * as _ from 'lodash'; 
import { WidgetTableService } from './widget-table.service';
import { WidgetTableConstant } from './widget-table.constant';

export class WidgetTableController {
  static $inject = ['widgetTableService', 'widgetTableConstant'];

  gridOptions: any;
  widget: string;
  property: string;
  widgetName: string;
  data: object;
  private namebtn: string = this.widgetTableConstant.previous;
  private index = 0;

  constructor(
    private widgetTableService: WidgetTableService,
    private widgetTableConstant: WidgetTableConstant
  ) {};
  
  $onChanges() {
    this.widgetName = _.upperFirst(this.widget);
    this.gridOptions = this.widgetTableService.getGridOptions(this.widget, this.index, this.property, this.namebtn);
    this.gridOptions.data = [this.data];
  }

  previousLatest() {
    this.namebtn = (this.namebtn === this.widgetTableConstant.previous) ? this.widgetTableConstant.latest : this.widgetTableConstant.previous;
    this.index = (this.namebtn === this.widgetTableConstant.latest) ? 1 : 0;
    this.gridOptions.columnDefs = this.widgetTableService.getColumnDefs(this.widget, this.index, this.property, this.namebtn);
  }
}

  


  

