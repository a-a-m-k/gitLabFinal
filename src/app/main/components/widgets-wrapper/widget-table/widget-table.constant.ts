import { NamesBtn } from './widget-table.interfaces';

export class WidgetTableConstant implements  NamesBtn {
    previous: string;
    latest: string;

    static get constants(): any {
        return {
            previous: 'previous',
            latest: 'latest'
        };
    }
  }
