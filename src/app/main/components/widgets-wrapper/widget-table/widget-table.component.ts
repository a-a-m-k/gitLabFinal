import './widget-table.component.scss';
import { WidgetTableController } from './widget-table.controller';

export const WidgetTableComponent: angular.IComponentOptions = {
    bindings: {
        widget: '<',
        property: '<',
        data: '<'
    }, 
    template: require('./widget-table.component.html'),
    controller: WidgetTableController
};
