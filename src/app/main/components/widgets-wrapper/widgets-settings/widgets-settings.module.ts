import * as angular from 'angular';
import { widgetsSettingsComponent } from './widgets-settings.component';


export const widgetsSettingsModule = angular
  .module('app.main.widgetsSettings', [])
  .component('widgetsSettingsComponent', widgetsSettingsComponent)
  .constant('settingConst', ['medication', 'diagnoses'])
  .name;

