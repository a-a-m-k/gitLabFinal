import './widgets-settings.component.scss';

import { WidgetsSettingsController } from './widgets-settings.controller';

export const widgetsSettingsComponent: angular.IComponentOptions = {
    bindings: {
        onSelected: '&'
    }, 
    template: require('./widgets-settings.component.html'),
    controller: WidgetsSettingsController
};
