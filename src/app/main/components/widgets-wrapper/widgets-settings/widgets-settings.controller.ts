import * as angular from 'angular';
import * as _ from 'lodash';

export class WidgetsSettingsController {  
  static $inject = ['$mdDialog', 'settingConst'];
  constructor(    
    private $mdDialog: any,
    private settingConst: string[]   
  ) {};
  items = [...this.settingConst];
  selected = [...this.settingConst];
  widgetsStatus = false;
  onSelected: Function;
  
  showTabDialog() {  
    this.$mdDialog.show({
    contentElement: '#widgetsDialog',
    parent: angular.element(document.body)
    });
  };
  
  toggleDialog(state) {
    this.$mdDialog.hide();
    this.widgetsStatus = state;
  }

  toggleWidgetsCheckbox(item) {
    const add = () => this.selected.concat(item);   
    const remove = () => _.remove(this.selected, item);   
    this.selected = this.exists(item, this.selected) ? remove() : add();
  }
  
  exists(item, list) {
    return _.includes(list, item);
  }
  select() {
    this.onSelected({
        selected: this.selected
    });
  }
}

