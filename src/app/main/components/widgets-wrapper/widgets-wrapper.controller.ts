import * as _ from 'lodash'; 
import { WidgetsData } from './widgets-wrapper.constant';

export class WidgetsWrapperController {
  static $inject = ['widgetsData'];

  properties;
  selected: string[];
  selectedPatient: object;

  constructor(
    private widgetsData: WidgetsData
  ) {};
  
  $onInit() {
    this.properties = this.widgetsData;
    this.selected = [this.widgetsData[0].widget, this.widgetsData[1].widget];
  }

  activeWidget(widget) {
    return _.includes(this.selected, widget);
  }
  selectedWidgets(selected) {
    this.selected = selected;
  }
  
}

  


  

