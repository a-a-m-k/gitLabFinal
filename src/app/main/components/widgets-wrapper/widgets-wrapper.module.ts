import * as angular from 'angular';
import { WidgetsWrapperComponent } from './widgets-wrapper.component';
import { WidgetsData } from './widgets-wrapper.constant';
import { widgetTableModule } from './widget-table/widget-table.module';
import { widgetsSettingsModule } from './widgets-settings/widgets-settings.module';
import ngMaterial  = require('angular-material');
import uiGrid = require('angular-ui-grid');
import 'angular-material/angular-material.css';
import 'angular-ui-grid/ui-grid.css';

export const widgetsWrapperModule = angular
  .module('app.main.widgetsWrapper', [ widgetTableModule, widgetsSettingsModule, ngMaterial ])
  .component('widgetsWrapperComponent', WidgetsWrapperComponent)
  .constant('widgetsData', WidgetsData.constants)
  .name;
