import './widgets-wrapper.component.scss';
import { WidgetsWrapperController } from './widgets-wrapper.controller';

export const WidgetsWrapperComponent: angular.IComponentOptions = {
    bindings: {
        selectedPatient: '<'
    },  
    template: require('./widgets-wrapper.component.html'),
    controller: WidgetsWrapperController
};
