export class WidgetsData {
  
  static get constants(): any {
    return [
      {
        widget: 'medication',
        property: 'drugs'
      },
      {
        widget: 'diagnoses',
        property: 'diagnose'
      }
    ];
  }
}
