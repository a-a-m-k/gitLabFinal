export const PacientIconColor = () => {
  return {
    restrict: 'A',
    scope: {
      backgroundColor: '@color'
    },
    link($scope, $element, $attrs) {  
        $element.css('background-color', $scope.backgroundColor);
        $attrs.$observe('color', (value) => {
          $element.css('background-color', value);
        });        
    }
  };
};
