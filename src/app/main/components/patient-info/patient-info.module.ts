import * as angular from 'angular';
import { PatientInfoComponent } from './patient-info.component';
import { PacientIconColor } from './patient-info.directive';
import { widgetsWrapperModule } from '../widgets-wrapper/widgets-wrapper.module';

export const patientInfoModule = angular
  .module('app.main.components.info', [ widgetsWrapperModule ])
  .component('patientInfoComponent', PatientInfoComponent)
  .directive('pacientIconColor', PacientIconColor)
  .name;
