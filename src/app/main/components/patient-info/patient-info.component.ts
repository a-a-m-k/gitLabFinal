import './patient-info.component.scss';

import { PatientInfoController } from './patient-info.controller';

export const PatientInfoComponent: angular.IComponentOptions = {
    bindings: { patient: '<' },
    template: require('./patient-info.component.html'),
    controller: PatientInfoController
};
