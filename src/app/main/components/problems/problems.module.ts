import * as angular from 'angular';
import { problemsRoutes } from './problems.config';
import { ProblemsComponent } from './problems.component';
import { ProblemsService } from './problems.service';

export const problemsModule = angular
  .module('app.main.problems', [])
  .component('problemsComponent', ProblemsComponent)
  .config(problemsRoutes)
  .service('ProblemsService', ProblemsService)
  .name;
