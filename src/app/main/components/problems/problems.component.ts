import { ProblemsController } from './problems.controller';

export const ProblemsComponent: angular.IComponentOptions = {
  template: require('./problems.component.html'),
  controller: ProblemsController
};
