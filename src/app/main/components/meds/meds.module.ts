import * as angular from 'angular';
import { medsRoutes } from './meds.config';
import { MedsComponent } from './meds.component';
import { MedsService } from './meds.service';


export const medsModule = angular
  .module('app.main.meds', [])
  .component('medsComponent', MedsComponent)
  .config(medsRoutes)
  .service('MedsService', MedsService)
  .name;
