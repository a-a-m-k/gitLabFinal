import { MedsService } from './meds.service';

export class MedsController {
  public static $inject = ['MedsService'];

  constructor(
    private medsService: MedsService
  ) {};

  public $onInit() { }

}
