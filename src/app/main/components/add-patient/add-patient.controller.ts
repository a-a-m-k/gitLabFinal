import * as angular from 'angular';
import { Patient, Abbreviation} from './add-patient.interface';
import { Constants } from './add-patient.constant';

export class AddPatientController {  
  static $inject = ['$mdDialog', 'AbbrevProperties'];

  patient: Patient;
  states: Abbreviation['states'];
  sexes: Abbreviation['sexes'];
  showHints;
  minDate;
  maxDate;
  onCreatePatient;
  constants;
  
  constructor(private $mdDialog: any, constants: Constants) {
    this.constants = constants;
  };

  $onInit() {
    this.showHints = true;
    this.minDate = new Date();
    this.maxDate = new Date();
    
    this.states = this.getDropDownList(this.constants.states);
    this.sexes = this.getDropDownList(this.constants.sexes);
  }

  $onChanges(changes) {
    if (changes.patient) {
      this.patient = Object.assign({}, this.patient);
    }
  }
  
  onSubmit() {
    this.onCreatePatient({
      $event: {
        patient: this.patient
      }
    });
    this.$mdDialog.hide();
  }
  
  getDropDownList(arr) {
    return arr.reduce((s, v) => s.concat({abbrev: v}), []);
  }

  showForm() { 
    this.$mdDialog.show({
    contentElement: '#formDialog',
    parent: angular.element(document.body)
    });
  };
  
  toggleForm(state) {
    this.$mdDialog.hide();
  }
}


