import * as angular from 'angular';
import { addPatientComponent } from './add-patient.component';
import { Constants } from './add-patient.constant';

export const addPatientModule = angular
  .module('app.main.addPatient', [])
  .component('addPatientComponent', addPatientComponent)
  .constant('AbbrevProperties', Constants.Default)
  .name;
