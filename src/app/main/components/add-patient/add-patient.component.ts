import './add-patient.component.scss';
import { AddPatientController } from './add-patient.controller';

export const addPatientComponent: angular.IComponentOptions = {
  bindings: {
    patient: '<',
    onCreatePatient: '&'
  },
  template: require('./add-patient.component.html'),
  controller: AddPatientController
};
