import * as angular from 'angular';
import { addPatientModule } from './add-patient/add-patient.module';
import { medsModule } from './meds/meds.module';
import { problemsModule } from './problems/problems.module';
import { tabsModule } from './nav-tabs/tabs.module';
import { patientInfoModule } from './patient-info/patient-info.module';
import { showPatientsListModule } from './show-patients-list/show-patients-list.module';
import {searchModule} from './patient-search/patient-search.module';
import {headerModule} from './header/header.module';

export const componentsModule = angular
  .module('app.main.component', [
    addPatientModule,
    tabsModule,
    medsModule,
    problemsModule,
    patientInfoModule,      
    showPatientsListModule,    
    searchModule,
    headerModule
  ])
  .name;
