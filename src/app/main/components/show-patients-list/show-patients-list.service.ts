import * as _ from 'lodash';

export class ShowPatientsListService {

  sortedpatients;

  sortPatientsList(patients) {
    this.sortedpatients = _.sortBy(patients, 'firstName');
    return this.sortedpatients;
  }
}
