import * as angular from 'angular';
import { showPatientsListComponent } from './show-patients-list.component';
import { ShowPatientsListService } from './show-patients-list.service';

export const showPatientsListModule = angular
  .module('app.show-patients-list', [])
  .component('showPatientsListComponent', showPatientsListComponent)
  .service('showPatientsListService', ShowPatientsListService)
  .name;
