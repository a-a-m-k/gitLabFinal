import { ShowPatientsListService } from './show-patients-list.service';

export class ShowPatientsListController {

  static $inject = ['showPatientsListService'];
  patients;
  sortedPatients;
  onSelectPatient: Function;

  constructor(
    private showPatientsListService: ShowPatientsListService
  ) {};

  $onChanges(changes) {
    if (changes.patients.currentValue) {
     this.sortedPatients = this.showPatientsListService.sortPatientsList(this.patients);
    }
  }
 
  selectPatient(index) {
    this.onSelectPatient({index});
  }
}
