import './show-patients-list.component.scss';
import { ShowPatientsListController } from './show-patients-list.controller';

export const showPatientsListComponent: angular.IComponentOptions = {
  bindings: {
    patients: '<',
    onSelectPatient: '&'
  },
  template: require('./show-patients-list.component.html'),
  controller: ShowPatientsListController
};
