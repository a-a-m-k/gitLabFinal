import { SearchController } from './patient-search.controller';

export const searchComponent: angular.IComponentOptions = {
  bindings: {
    patients: '<',
    onChangeFilter: '&'
  },
  template: require('./patient-search.component.html'),
  controller: SearchController
  };
