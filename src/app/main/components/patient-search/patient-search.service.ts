import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as Rx from 'rxjs/Rx';
import * as _ from 'lodash';

export class SearchService {
  static $inject = ['$http'];
  patients$;

  constructor(private $http) {
  }

  filterPatient(dataArray, searchString) {
    const term = _.lowerCase(searchString);

    return _.filter(dataArray, item => {
      const { firstName, lastName } = item;
      return _.includes(_.lowerCase(`${firstName} ${lastName}`), term);
    });
  }
}

