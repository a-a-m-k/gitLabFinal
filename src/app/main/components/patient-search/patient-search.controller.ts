import { SearchService } from './patient-search.service';
import * as Rx from 'rxjs/Rx';

export class SearchController {
  static $inject = ['searchService'];
  el = document.getElementById('SearchBox');
  keyups$;
  patients;

  constructor(private searchService: SearchService) {    
  }

  $onInit() {
    Rx.Observable.fromEvent(this.el, 'keyup')
      .map((event: any) => event.target.value)
      .map(value => this.searchService.filterPatient(this.patients, value))
      .subscribe(res => this.onChangeFilter({ res }));
  }   
}
