import * as angular from 'angular';
import { searchComponent } from './patient-search.component';
import { SearchService } from './patient-search.service';

export const searchModule = angular
  .module('app.search', [])
  .component('searchComponent', searchComponent)
  .service('searchService', SearchService)
  .name;
