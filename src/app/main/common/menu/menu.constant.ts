import { MenuItemsInterface, MenuStateInterface } from './menu.interface';

export class MenuData {

    static get menuState(): MenuStateInterface {
        return {
            opened: 'keyboard_arrow_left',
            closed: 'keyboard_arrow_right'
        };     
    };

    static get menuItemsData(): MenuItemsInterface {
        return [
            {
                name: 'Overview',
                icon: 'poll',
                url: 'overview'
            },
            {
                name: 'Patients',
                icon: 'people',
                url: 'patients'
            },
            {
                name: 'Admin',
                icon: 'account_circle',
                url: 'admin'
            }
        ];   
            
    };
}
