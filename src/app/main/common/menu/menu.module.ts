import * as angular from 'angular';
import { MenuComponent } from './menu.component';
import { MenuData } from './menu.constant';

export const menuModule = angular
  .module('app.main.menu', [])
  .component('menuComponent', MenuComponent)
  .constant('menuData', MenuData)
  .name;
