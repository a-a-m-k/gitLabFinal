export class MenuController {
  static $inject = ['$mdSidenav', 'menuData'];
  menuState: string;
  menuItemsData: any;

  constructor(
    private $mdSidenav: any,
    private menuData: any
  ) { };

  $onInit() {
    this.menuState = this.menuData.menuState.closed;
    this.menuItemsData = this.menuData.menuItemsData;
  }

  toggleLeft() {
    this.buildToggler('sidebar');
    this.changeMenuState(this.menuState);
  }
  
  buildToggler(componentId) {
    this.$mdSidenav(componentId).toggle();
  }

  changeMenuState(state) {
    this.menuState = state === this.menuData.menuState.closed ? this.menuData.menuState.opened : this.menuData.menuState.closed;
  }
}
