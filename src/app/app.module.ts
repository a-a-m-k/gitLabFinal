import * as angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import { appRoutes } from './app.config';
import { AppComponent } from './app.component';
import { mainModule } from './main/main.module';
import  ngMaterial  = require('angular-material');
import 'angular-material/angular-material.css';

export const root = angular
  .module('app', [
    ngMaterial,
    uiRouter,
    mainModule
])
  .component('app', AppComponent)
  .config(appRoutes)
  .name;
